package persist.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
class Employee {

    private String name;
    private int age;
    private String position;
    private int salary;
    private List<Skill> skills;
    
    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    @XmlElement
    public void setAge(int age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }

    @XmlElement
    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    @XmlAttribute
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
