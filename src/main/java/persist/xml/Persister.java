package persist.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Persister {
    
    public static void main(String[] args) throws JAXBException {
        Employee empl = new Employee();
        
        empl.setName("Homer Simpson");
        empl.setAge(40);
        empl.setPosition("Nuclear Safety Inspector");
        empl.setSalary(12000);
        
        List<Skill> skills = new ArrayList<>();
        
        skills.add(new Skill("Drink beer", 5));
        skills.add(new Skill("Eat donut", 5));
        skills.add(new Skill("Inspect power plant", 1));
        empl.setSkills(skills);
        
        File file = new File("file.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Employee.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(empl, file);
        jaxbMarshaller.marshal(empl, System.out);
        
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Employee empl2 = (Employee) jaxbUnmarshaller.unmarshal(file);
        System.out.println("Name " + empl2.getName() + " Age " + empl2.getAge() + " Position " + empl2.getPosition());
    }

}
