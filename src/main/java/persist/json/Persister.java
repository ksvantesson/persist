package persist.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Persister {
    public static void main(String[] args) throws IOException {
      
        Employee empl = new Employee();
        
        empl.setName("Homer Simpson");
        empl.setAge(40);
        empl.setPosition("Nuclear Safety Inspector");
        empl.setSalary(12000);
        
        List<Skill> skills = new ArrayList<>();
        
        skills.add(new Skill("Drink beer", 5));
        skills.add(new Skill("Eat donut", 5));
        skills.add(new Skill("Inspect power plant", 1));
        empl.setSkills(skills);

        ObjectMapper mapper = new ObjectMapper();
        
        //Object to JSON in file
        mapper.writeValue(new File("file.json"), empl);

        //Object to JSON in String
        String jsonInString = mapper.writeValueAsString(empl);
        System.out.println(jsonInString);
        
        jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(empl);
        System.out.println(jsonInString);
        
        Employee empl2 = mapper.readValue(new File("file.json"), Employee.class);
        System.out.println("Name " + empl2.getName() + " Age " + empl2.getAge() + " Position " + empl2.getPosition());
    }
}
