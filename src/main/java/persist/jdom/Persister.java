package persist.jdom;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.DataConversionException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class Persister {
    public static void main(String[] args) {
        
      
        Employee empl = new Employee();
        
        empl.setName("Homer Simpson");
        empl.setAge(40);
        empl.setPosition("Nuclear Safety Inspector");
        empl.setSalary(12000);
        
        List<Skill> skills = new ArrayList<>();
        
        skills.add(new Skill("Drink beer", 5));
        skills.add(new Skill("Eat donut", 5));
        skills.add(new Skill("Inspect power plant", 1));
        empl.setSkills(skills);
        
        try {
        
            XMLOutputter xmlOutput = new XMLOutputter();

            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(createDoc(empl), new FileWriter("file2.xml"));
        }
        catch (IOException e) {
        }
        
        try {
            SAXBuilder builder = new SAXBuilder();
            File xmlFile = new File("file2.xml");
            Document doc = builder.build(xmlFile);
            
            Employee empl2 = read(doc.getRootElement());
            System.out.println("Name " + empl2.getName() + " Age " + empl2.getAge() + " Position " + empl2.getPosition());
        }
        catch (IOException | JDOMException e) {
        }
    }

    private static Document createDoc(Employee empl) {
        
        Element emplElement = new Element("employee");
        
        emplElement.setAttribute("salary", Integer.toString(empl.getSalary()));
        
        Element ageElement = new Element("age");
        
        ageElement.setText(Integer.toString(empl.getAge()));
        emplElement.addContent(ageElement);

        Element nameElement = new Element("name");
        
        nameElement.setText(empl.getName());
        emplElement.addContent(nameElement);
        
        Element positionElement = new Element("position");
        
        positionElement.setText(empl.getPosition());
        emplElement.addContent(positionElement);
        
        Element skillsElement = new Element("skills");
        
        for (Skill skill : empl.getSkills()) {
            Element skillElement = new Element("skill");
            Element levelElement = new Element("level");
            Element skillNameElement = new Element("name");

            levelElement.setText(Integer.toString(skill.getLevel()));
            skillNameElement.setText(skill.getName());
            
            skillElement.addContent(levelElement);
            skillElement.addContent(skillNameElement);
            skillsElement.addContent(skillElement);
        }
        
        emplElement.addContent(skillsElement);
        
        Document doc = new Document();
        
        doc.setRootElement(emplElement);
        
        return doc;
    }

    private static Employee read(Element emplElement) throws DataConversionException {
        
        Employee empl = new Employee();
        
        empl.setSalary(emplElement.getAttribute("salary").getIntValue());
        empl.setAge(Integer.parseInt(emplElement.getChildText("age")));
        empl.setName(emplElement.getChildText("name"));
        empl.setPosition(emplElement.getChildText("position"));
        
        List<Skill> skills = new ArrayList<>();
        
        for (Element skillElement : emplElement.getChild("skills").getChildren("skill")) {
            Skill skill = new Skill(skillElement.getChildText("name"), Integer.parseInt(skillElement.getChildText("level")));
            
            skills.add(skill);
        }
        
        empl.setSkills(skills);
        
        return empl;
    }
}
