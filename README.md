# Utlåningssystem för biblioteket #

Du ska bygga ett kommandotolksbaserat system för utlåning av böcker.

## Operationer ##

Systemet ska tillhandahålla nedanstående operationer. Dessa är tänkta att användas av bibliotekarien.

1. Registrera nya böcker
    * Författare, titel och kategori
    * I samband med registreringen ger systemet boken ett unikt siffer-ID
1. Registrera nya låntagare
    * Namn
    * I samband med registreringen ger systemet låntagaren ett unikt siffer-ID
1. Registrera lån
    * Bok-ID och låntagar-ID
    * Systemet räknar ut återlämningsdatum
1. Registrera återlämning
1. Visa bok (givet bok-ID)
    * Författare, titel och kategori
    * Eventuell utlåning (låntagare och återlämningsdatum)
1. Visa låntagare (givet låntagar-ID)
    * Namn
    * Lista eventuella lånade böcker
1. Söka bok/böcker
    * Författare
    * Titel
    * Kategori
1. Lista samtliga utlånade böcker

### Begränsningar ###

Antalet böcker som en låntagare får ha hemma är begränsat till maximalt *fem* stycken. En låntagare som ej lämnat tillbaka en bok vars återlämningsdatum har passerat, får ej låna ytterligare böcker. Det är först när boken har lämnats tillbaka som ytterligare lån medges.

Systemet ska hantera att en sökning resulterar i noll, en eller flera träffar. *Tips: Använd `contains` på strängar för att avgöra strängen innehåller en delsträng.*

### Exempel ###

Nedan visas ett exempel på hur det skulle kunnna se ut när användaren registerar en ny bok.


    1 - Registrera bok
    2 - Registrera låntagare
    3 - Registrera lån
    4 - Registrera återlämning
    5 - Sök
    6 - Avsluta

    Vad vill du göra? > 1

    Registrera ny bok

    Ange författare: Robert C. Martin
    Ange titel: Clean Code
    Ange kategori: Programmering

    Boken är nu registrerad. Den har ID #23.

***

# *Extrauppgifter* #

Om du har tid över får du gärna implementera föjande extrauppgifter.

## *Extrauppgift:* Spara ##

Använd t. ex. XML för att spara all information i utlåningssystemet. I samband med att systemet avslutar kan det vara lämpligt att spara informationen. Eventuellt kan det vara lättare att spara informationen genom att skapa särskilda dataklasser som endast används vid inläsning och sparning.

Se källkoden till det här projektet för exempel på hur man läser och skriver XML.

## *Extrauppgift:* Självbetjäning ##

Gör en självbetjäningsvariant av systemet som låntagare kan använda.

### Operationer ###

Vissa operationer är samma som i huvudsystemet. Andra är snarlika men med vissa begränsningar.

1. Logga in
    * Görs genom att ange namnet, inget lösenord krävs
1. Söka böcker
    * Författare
    * Titel
    * Kategori
1. Visa bok (givet bok-ID)
    * Författare, titel och kategori
    * Eventuell utlåning (*endast återlämningsdatum*)
1. Registrera lån
1. Registrera återlämning
1. Lista lånade böcker
1. Logga ut